package com.projet.monitoringkitchen;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.VideoView;

import com.projet.monotoringkitchen1.R;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        
		setContentView(R.layout.activity_main);
		
		//Hide the status Bar
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		getWindow().setBackgroundDrawableResource(R.color.background_color);
		
		  Typeface font = Typeface.createFromAsset(getAssets(),
	                "fonts/Roboto-MediumItalic.ttf");
	        Button b1 = (Button) findViewById(R.id.emergency);
	        b1.setTypeface(font);
	        
	        Button b2 = (Button) findViewById(R.id.eat);
	        b2.setTypeface(font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		menu.add(0, 0, 0, "time")
		.setActionView(findViewById(R.id.digital))
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS); 

		  // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.activity_main, menu);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.submenu_contact:
	        	Intent intent1=new Intent(MainActivity.this, ListContactActivity.class);
	        	startActivity(intent1);
	        	return true;
	        //case R.id.submenu_etat_capteur:
	        	//Intent intent2=new Intent(MainActivity.this, ListCapteurActivity.class);
	        	//startActivity(intent2);
	        	//return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
}
