package com.projet.monitoringkitchen;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.projet.monotoringkitchen1.R;

public class ListContactActivity extends ListActivity {

	private static final String DATABASE_NAME = "db";
	private static final String TABLE_NAME="contacts";
	private static final String KEY="_id";
	static final String NOM = "nom";
	static final String PRENOM = "prenom";
	static final String TELEPHONE = "telephone";
	static final int REQUEST_IMAGE_CAPTURE = 1;
	private static int position;
	
	private SQLiteDatabase db = null;
	private Cursor contactCursor = null;
	
	private SQLiteDatabase db_bis = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Hide the status Bar
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		getWindow().setBackgroundDrawableResource(R.color.background_color);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.list_contact_activity);

		db = (new ContactDatabaseHelper(this)).getWritableDatabase();

		contactCursor = db.rawQuery("SELECT _id," + NOM + "," + PRENOM + ","
				+ TELEPHONE + " FROM "+ TABLE_NAME +" ORDER BY " + KEY, null);

		ListAdapter adapter = new SimpleCursorAdapter(this,
				R.layout.list_contact_row, contactCursor, new String[] {NOM,
						PRENOM, TELEPHONE }, new int[] { R.id.name_bis, R.id.firstname_bis, R.id.phone_bis});
		
		ListView list = getListView();
		list.setOnItemClickListener( new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(ListContactActivity.this, Integer.toString(position) , Toast.LENGTH_LONG)
				.show();
				
				ListContactActivity.this.position=position;
				
				CharSequence parameters[] = new CharSequence[] {
						getString(R.string.see_contact),
						getString(R.string.delete_contact) };
				AlertDialog.Builder builder = new AlertDialog.Builder(ListContactActivity.this);
				builder.setTitle(getString(R.string.action_to_do));
				builder.setItems(parameters, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// the user clicked on colors[which]

						switch (which) {
						case 0:
							break;
						case 1 : {
							//prendre l'_id dans la bas de donn�e et la supprimer
							processDelete(ListContactActivity.this.position+1);
						break;
						}
						}
					}
				});
				builder.show();

			}
		});

		setListAdapter(adapter);
		registerForContextMenu(getListView());

		ActionBar myActionBar = getActionBar();

		myActionBar.setDisplayShowTitleEnabled(false);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		contactCursor.close();
		db.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.list_contact_activity, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.add_contact:
			add();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void add() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View addView = inflater.inflate(R.layout.contact_register_activity,
				null);
		final DialogWrapper wrapper = new DialogWrapper(addView);

		new AlertDialog.Builder(this)
				.setTitle(R.string.add_contact)
				.setView(addView)
				.setPositiveButton(R.string.contact_register,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								processAdd(wrapper);
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// ignore, just dismiss
							}
						}).show();
	}

	// Affichage pop ip menu
	public void showPopupWindowPhoto(View v) {
		// Create a popup that has multiple selection options

		CharSequence parameters[] = new CharSequence[] {
				this.getString(R.string.picture_in_gallery),
				this.getString(R.string.picture_with_camera) };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose in");
		builder.setItems(parameters, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// the user clicked on colors[which]

				switch (which) {
				case 0: {
					Intent i = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(i, 0);
					break;
				}
				case 1 : {
					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
				        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
				    }
				}

				}
			}
		});
		builder.show();

	}

	private void processAdd(DialogWrapper wrapper) {
		
		ContentValues values = new ContentValues();
		
		values.put(NOM, wrapper.getNom());
		values.put(PRENOM, wrapper.getPrenom());
		values.put(TELEPHONE, wrapper.getTelephone());

		db.insert(TABLE_NAME, null, values);

		contactCursor.requery();
	}

	private void processDelete(int rowId) {
		String[] args = { String.valueOf(rowId) };

		db.delete(TABLE_NAME, "_id=?", args);
		contactCursor.requery();
	}

	class DialogWrapper {
		EditText nom = null;
		EditText prenom = null;
		EditText telephone = null;
		View base = null;

		DialogWrapper(View base) {
			this.base = base;
			nom = (EditText) base.findViewById(R.id.nom_contact);
			prenom = (EditText) base.findViewById(R.id.prenom_contact);
			telephone = (EditText) base.findViewById(R.id.telephone_contact);

		}

		String getNom() {
			return (getNomField().getText().toString());
		}

		String getPrenom() {
			return (getPrenomField().getText().toString());
		}

		String getTelephone() {
			return (getTelephoneField().getText().toString());
		}

		private EditText getNomField() {
			if (nom == null) {
				nom = (EditText) base.findViewById(R.id.nom_contact);
			}

			return (nom);
		}

		private EditText getPrenomField() {
			if (prenom == null) {
				prenom = (EditText) base.findViewById(R.id.prenom_contact);
			}

			return (prenom);
		}

		private EditText getTelephoneField() {
			if (telephone == null) {
				telephone = (EditText) base
						.findViewById(R.id.telephone_contact);
			}

			return (telephone);
		}
	}
}
