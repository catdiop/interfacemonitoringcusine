package com.projet.monitoringkitchen;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactDatabaseHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME="db";
	static final String NOM="nom";
	static final String PRENOM="prenom";
	static final String TELEPHONE="telephone";
	
	public static final String TABLE_CREATE="CREATE TABLE contacts (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
			NOM +" TEXT NOT NULL, " +
			PRENOM +" TEXT NOT NULL, " +
			TELEPHONE +" INTEGER);";
	
	public static final String TABLE_DROP="DROP TABLE IF EXISTS contacts";
	
	public ContactDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		android.util.Log.w(DATABASE_NAME, "Maj de la base, suppression de toutes les anciennes donn�es");
		db.execSQL(TABLE_DROP);
		onCreate(db);
	}
}
